package timetrakkr

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TimetrakkrApplication

fun main(args: Array<String>) {
	runApplication<TimetrakkrApplication>(*args)
}
